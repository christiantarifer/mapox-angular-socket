import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';

// ***************************************************************************************** //

import { SocketIoModule } from 'ngx-socket-io';
import { MapaComponent } from './components/mapa/mapa.component';
import { environment } from '../environments/environment';



// ***************************************************************************************** //

@NgModule({
  declarations: [
    AppComponent,
    MapaComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    SocketIoModule.forRoot( environment.socketConfig )
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
