import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';


// ***************************************************************************************** //

import { Lugar } from '../../interfaces/interfaces';

// ***************************************************************************************** //

import * as mapboxgl from 'mapbox-gl';
import { WebsocketService } from '../../services/websocket.service';

// ***************************************************************************************** //

interface RespuestaMarcadores {

  [ key: string]: Lugar;

}

@Component({
  selector: 'app-mapa',
  templateUrl: './mapa.component.html',
  styleUrls: ['./mapa.component.css']
})
export class MapaComponent implements OnInit {

  mapa: mapboxgl.Map;

  // lugares: Lugar[] = [];
  lugares: RespuestaMarcadores = {};
  markersMapbox: { [ id: string ]: mapboxgl.Marker } = {};

  constructor(
    private http: HttpClient,
    private wsService: WebsocketService
  ) { }

  ngOnInit(): void {

    this.http.get<RespuestaMarcadores>('http://localhost:5000/mapa').subscribe(

      lugares => {

        this.lugares = lugares;
        this.crearMapa();

      },

      error => {
        console.log(error);
      }


    );

    this.escucharSockets();

  }

  // ***************************************************************************************** //

  escucharSockets() {

    // NEW MARKER
    this.wsService.listen('marcador-nuevo')
    .subscribe( (marcador: Lugar) => this.agregarMarcador( marcador ) );

    // TODO: MOVEMENT MARKET
    // market instance setLngLat([ lng, lat ])
    this.wsService.listen('marcador-mover')
    .subscribe( (marcador: Lugar) => {

      this.markersMapbox[ marcador.id ].setLngLat([ marcador.lng, marcador.lat ]);

    });

    // DELETE MARKER
    this.wsService.listen('marcador-borrar')
    .subscribe( (id: string) => {

      this.markersMapbox[id].remove();
      delete this.markersMapbox[id];

    });

  }

  // ***************************************************************************************** //

  crearMapa(): void {

    (mapboxgl as any ).accessToken = 'pk.eyJ1Ijoib3duY3JlZWQ5MyIsImEiOiJja2gwcTJyd28xM3c4MnlwY28xeHlxaTB3In0.XNIkeyNsIOGh4ltAjoI45g';

    this.mapa = new mapboxgl.Map({
      container: 'mapa',
      style: 'mapbox://styles/mapbox/streets-v11',
      center: [-75.75512993582937, 45.349977429009954],
      zoom: 15.8
    });

    for ( const [id, marcador] of Object.entries(this.lugares) ) {


      this.agregarMarcador( marcador );

    }


  }

  // ***************************************************************************************** //

  agregarMarcador( marcador: Lugar ): void {

    // const html = `
    // <h2>${ marcador.nombre }</h2>
    // <br />
    // <button>Borrar</button>
    // `;

    const h2 = document.createElement('h2');
    h2.innerText = marcador.nombre;

    const btnBorrar = document.createElement('button');
    btnBorrar.innerText = 'Borrar';

    const div = document.createElement('div');

    div.append( h2, btnBorrar );


    const customPopUp = new mapboxgl.Popup({
      offset: 25,
      closeOnClick: true
    }).setDOMContent( div );

    const marker = new mapboxgl.Marker({
      draggable: true,
      color: marcador.color
    })
    .setLngLat( [marcador.lng, marcador.lat] )
    .setPopup( customPopUp )
    .addTo(this.mapa);


    marker.on('drag', () => {

      const lngLat = marker.getLngLat();

      const nuevoMarcador = {

        id: marcador.id,
        ...lngLat

      };

      // console.log( lngLat );
      // console.log( nuevoMarcador );

      // TODO: CREATE EVENTO TO EMIT MARKER'S COORDINATES
      // marcador-mover
      this.wsService.emit( 'marcador-mover', nuevoMarcador );


    });

    btnBorrar.addEventListener( 'click', () => {

      marker.remove();

      // TODO: DELETE MARKER THROUGH SOKETS
      this.wsService.emit( 'marcador-borrar', marcador.id );

    });

    this.markersMapbox[ marcador.id ] = marker;
    console.log( this.markersMapbox );

  }

  // ***************************************************************************************** //

  crearMarcador(): void {

    const customMarker: Lugar = {
      id: new Date().toISOString(),
      lng: -75.75512993582937,
      lat: 45.349977429009954,
      nombre: 'Sin nombre',
      color: '#' + Math.floor(Math.random() * 16777215).toString(16)
    };

    this.agregarMarcador( customMarker );

    // EMITT NEW MARKER
    this.wsService.emit('marcador-nuevo', customMarker );

  }

  // ***************************************************************************************** //

}
